#include <iostream>
#include <string>
#include <time.h>  

#include "Utilities.h"
#include "Arena.h"
#include "RockAlwaysWins.h"
#include "NeverTheSame.h"
#include "BeatYourLast.h"
#include "Cyclops.h"
#include "YourNameRPS.h"
//#include "SpencerRPS.h"

using namespace std;

int main()
{
	srand (time(NULL));

	string input;
	Arena* arena = new Arena();

	arena->vAddPlayer(new Player("Random"));
	arena->vAddPlayer(new RockAlwaysWins());
	arena->vAddPlayer(new NeverTheSame());
	arena->vAddPlayer(new BeatYourLast());
	arena->vAddPlayer(new Cyclops());
	//arena->vAddPlayer(new SpencerRPS());
	arena->vAddPlayer(new YourNameRPS());

	do{
		Utilities::vPrintCenter("Welcome to Rock Paper Scissor AI Arena");
		cout << "0 - New Game" << endl;
		cout << "1 - Quit" << endl;
		getline(cin, input);
		system("cls");
		if(input[0] == '0'){
			arena->vStart();
		}
	}
	while(input[0] != '1');

	cout << "Written by: Spencer Poon Woo" << endl;
	system("pause");

	delete arena;
	return 0;
}

