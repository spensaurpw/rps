#pragma once
#include "player.h"

/*
Never plays the same move twice in a row
*/
class NeverTheSame :public Player
{
public:
	NeverTheSame(void);
	~NeverTheSame(void);
	Move GetMove();

private:
	Move m_lastMove;
};

