#include "Arena.h"


Arena::Arena(void)
{
	m_poFirstPlayer = NULL;
	m_poSecondPlayer = NULL;
}


Arena::~Arena(void)
{
	m_aPlayers.clear();
}

void Arena::vStart(){
	int iRounds;

	while(!bSelectPlayers());

	Utilities::vPrintCenter("How many rounds?");
	cin >> iRounds;
	cin.ignore();

	vPlay(iRounds);
	system("pause");
	system("cls");
}

bool Arena::bSelectPlayers(){
	string input;
	int playerNum;

	m_poFirstPlayer = NULL;
	m_poSecondPlayer = NULL;

	Utilities::vPrintCenter("Select Players");
	vPrintPlayers();
	cout << "First Player: ";
	getline(cin, input);
	system("cls");
	playerNum = input[0] - '0';
	if(playerNum >= 0 && playerNum < m_aPlayers.size()){
		m_poFirstPlayer = m_aPlayers[playerNum];
	}

	Utilities::vPrintCenter("Select Players");
	vPrintPlayers();
	cout << "Second Player: ";
	getline(cin, input);
	system("cls");
	playerNum = input[0] - '0';
	if(playerNum >= 0 && playerNum < m_aPlayers.size()){
		m_poSecondPlayer = m_aPlayers[playerNum];
	}
	if(m_poFirstPlayer == m_poSecondPlayer){
		return false;
	}
	else if(m_poFirstPlayer && m_poSecondPlayer){
		return true;
	}

	return false;
}

void Arena::vPrintPlayers(){
	if(m_aPlayers.size() == 0){
		cout << "ERROR: There are no players in the arena!" << endl;
	}
	else{
		for(int i = 0; i < m_aPlayers.size(); i++){
			if(m_poFirstPlayer != m_aPlayers[i]){
				cout << i << " - " << m_aPlayers[i]->sGetName() << endl;
			}
		}
	}
}

void Arena::vAddPlayer(Player* poNewPlayer){
	m_aPlayers.push_back(poNewPlayer);
}

void Arena::vPlay(int iRounds){
	int tie = 0;
	int p1 = 0;
	int p2 = 0;

	m_poFirstPlayer->Start(iRounds);
	m_poSecondPlayer->Start(iRounds);

	for(int i = 0; i < iRounds; i++){
		Player::Move move1 = m_poFirstPlayer->GetMove();
		Player::Move move2 = m_poSecondPlayer->GetMove();

		if(move1 == move2){
			tie++;
			m_poFirstPlayer->Tie();
			m_poSecondPlayer->Tie();
		}
		else if(move1 == Player::ROCK && move2 == Player::PAPER){
			p2++;
			m_poFirstPlayer->Lose();
			m_poSecondPlayer->Win();
		}
		else if(move1 == Player::ROCK && move2 == Player::SCISSORS){
			p1++;
			m_poFirstPlayer->Win();
			m_poSecondPlayer->Lose();
		}
		else if(move1 == Player::PAPER && move2 == Player::ROCK){
			p1++;
			m_poFirstPlayer->Win();
			m_poSecondPlayer->Lose();
		}
		else if(move1 == Player::PAPER && move2 == Player::SCISSORS){
			p2++;
			m_poFirstPlayer->Lose();
			m_poSecondPlayer->Win();
		}
		else if(move1 == Player::SCISSORS && move2 == Player::ROCK){
			p2++;
			m_poFirstPlayer->Lose();
			m_poSecondPlayer->Win();
		}
		else if(move1 == Player::SCISSORS && move2 == Player::PAPER){
			p1++;
			m_poFirstPlayer->Win();
			m_poSecondPlayer->Lose();
		}
		else{
			cout << "ERROR" << endl;
			break;
		}

		cout << "\tTies: " << tie << "\t" << m_poFirstPlayer->sGetName() << ": " << p1 << "\t" << m_poSecondPlayer->sGetName() << ": " << p2 << endl;
//#define DEBUG
#ifdef DEBUG
		cout << "\t" << m_poFirstPlayer->sGetName() << ": ";
		if(move1 == Player::ROCK){
			cout << "ROCK";
		}
		else if(move1 == Player::SCISSORS){
			cout << "SCISSORS";
		}
		else if(move1 == Player::PAPER){
			cout << "PAPER";
		}
		cout << "\t\t" << m_poSecondPlayer->sGetName() << ": ";
		if(move2 == Player::ROCK){
			cout << "ROCK";
		}
		else if(move2 == Player::SCISSORS){
			cout << "SCISSORS";
		}
		else if(move2 == Player::PAPER){
			cout << "PAPER";
		}
		cout << endl;
#endif
	}

	cout << "\tTies: " << (int)(((float)tie/(float)iRounds)*100) << "%\t" << m_poFirstPlayer->sGetName() << ": " << (int)(((float)p1/(float)iRounds)*100) << "%\t" << m_poSecondPlayer->sGetName() << ": " << (int)(((float)p2/(float)iRounds)*100) << "%" << endl;
}

