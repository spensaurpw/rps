#pragma once
#include "player.h"
class BeatYourLast :public Player
{
public:
	BeatYourLast(void);
	~BeatYourLast(void);
	Move GetMove();
	void Win();
	void Lose();
	void Tie();

private:
	Move m_opponentPrev;
	Move m_myPrev;
};

